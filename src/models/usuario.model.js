import {pool} from '../../database/conexion.js';

class UsuarioModel{

    create =  async ( nombre, email, edad) =>{ 
        const [rows]  = await pool.query('INSERT INTO usuario(nombre, email, edad) VALUES (?,?,?);', [nombre, email, edad])
        return rows;
    }

    getAll= async() =>{
        const [rows] = await pool.query('SELECT * FROM usuario')
        return rows;
    }

    getOne = async(id) =>{
        const [rows] = await pool.query('SELECT * FROM usuario where id = ?',  [id])
        return rows;
    }

    update  = async(nombre, email, edad, id) =>{
        const [rows]  = await pool.query('UPDATE usuario SET nombre = ?, email = ?, edad = ?  WHERE id = ?', [nombre, email, edad, id])
        return rows;
    }

    delete = async(id) =>{
        const [rows] = await pool.query('DELETE FROM usuario where id = ?',  [id])
        return rows;
    }

}

export default UsuarioModel;