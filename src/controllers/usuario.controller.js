import UsuarioModel from '../models/usuario.model.js';
import  Request  from 'express';
import  Response  from 'express';

const usuarioModel = new UsuarioModel

class UsuarioController{
    create = async(Request, Response) => {
        try{  
            const {nombre, email, edad} =  Request.body
            const response = await usuarioModel.create(nombre, email, edad);
            Response.send(response)          
        }catch (error) {
            return Response.status(500).json({
                message: error
            })
        } 
    }

    index = async(Request, Response) =>{
        try{
            const response = await usuarioModel.getAll();
            Response.send (response)
        }catch (error) {
            return Response.status(500).json({
                message: error
            })
        } 
    }

    show = async(Request, Response) => {
        try{  
            const response = await usuarioModel.getOne(Request.params.id);
            Response.send (response)
        }catch (error) {
            return Response.status(500).json({
                message: error
            })
        } 
    }

    update = async(Request, Response) => {
        try{  
            const {nombre, email, edad} =  Request.body
            const response = await usuarioModel.update(nombre, email, edad, Request.params.id);
            Response.send(response)          
        }catch (error) {
            return Response.status(500).json({
                message: error
            })
        } 
    }

    delete = async(Request, Response) => {
        try{  
            const response = await usuarioModel.delete(Request.params.id);
            Response.send (response)
        }catch (error) {
            return Response.status(500).json({
                message: error
            })
        } 
    }
}

export default UsuarioController;