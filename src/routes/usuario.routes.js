import {Router} from 'express'
import UsuarioController from '../controllers/usuario.controller.js'

const router = Router();
const usuarioController = new UsuarioController;

router.post('/usuario', usuarioController.create)
router.get('/usuario', usuarioController.index, (req, res) => {
    res.render('index');
}) 
router.get('/usuario/:id', usuarioController.show) 
router.put('/usuario/:id', usuarioController.update) 
router.delete('/usuario/:id', usuarioController.delete) 


export default router