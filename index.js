import express from 'express'
import usuarioRoutes from './src/routes/usuario.routes.js';


const app = express()
const PORT = 5000;

app.use(express.json())

app.use(usuarioRoutes)

app.listen(5000)
console.log(`corriendo en el puerto:${PORT}`)
